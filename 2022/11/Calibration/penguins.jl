# Load packages
using CairoMakie
using CalibrationAnalysis
using CategoricalArrays
using DataFrames
using MLJ
using PalmerPenguins

using LinearAlgebra
using Random

# Plotting settings.
figpath(names...) = joinpath(@__DIR__, "figures", "penguins", names...)
mkpath(figpath())
savefig(name, fig=current_figure()) = save(figpath(name), fig)

# Load penguin data.
penguins = dropmissing(DataFrame(PalmerPenguins.load()))
y, X = unpack(
    penguins,
    ==(:species),
    x -> x === :flipper_length_mm || x === :body_mass_g;
    :species => Multiclass,
    :flipper_length_mm => MLJ.Continuous,
    :body_mass_g => MLJ.Continuous
)

# Create training and validation datasets.
trainidxs, validxs = partition(1:nrow(X), 0.7; stratify=y, shuffle=true, rng=Random.seed!(1234))
penguins.train = let validxs = validxs
    train = trues(nrow(X))
    train[validxs] .= false
    train
end

# Plot datasets.
let penguins = penguins, trainidxs = trainidxs, validxs = validxs
    # Create figure with two axes in one row.
    fig = Figure(; resolution=(800, 300))
    axtrain = Axis(fig[1, 1]; title="training (n = $(length(trainidxs)))", ylabel="body mass (g)")
    axval = Axis(
        fig[1, 2]; title="validation (n = $(length(validxs)))", yticksvisible=false, yticklabelsvisible=false
    )

    # Plot the training and validation data.
    for (idxs, ax) in ((trainidxs, axtrain), (validxs, axval))
        for (key, data) in pairs(groupby(penguins[idxs, :], :species))
            scatter!(
                ax,
                data.flipper_length_mm,
                data.body_mass_g;
                #markersize=6,
                label=string(key.species)
            )
        end
    end

    # Add common label for x axes.
    Label(fig[2, :], "flipper length (mm)"; valign=:top, padding=(0, 0, 0, 5))
    rowgap!(fig.layout, 1, 0)

    # Add legend.
    Legend(fig[1, 3], axtrain, "species"; framevisible=false, tellwidth=true)

    # Ensure that x- and y-axes of the both plots are the same.
    linkaxes!(axtrain, axval)

    fig
end
savefig("penguins.pdf")

# Train classification model with gradient boosting.
XGBoost = @load XGBoostClassifier pkg = XGBoost
model = fit!(machine(XGBoost(), X, y); rows=trainidxs)

# Compute confidence estimates and corresponding outcomes.
predictions_confidence = let
    predictions = MLJ.predict(model, X)
    mode = MLJ.predict_mode(model, X)
    confidence = Float64.(pdf.(predictions, mode))

    DataFrame(
        :id => 1:nrow(penguins),
        :train => penguins.train,
        :confidence => confidence,
        :outcomes => mode .== penguins.species,
    )
end

# Plot confidence estimates.
let data = predictions_confidence
    # Compute bins based on range of data.
    # Increase the last edge by eps() to ensure that the maximum values are included in the last bin.
    minconfidence, maxconfidence = extrema(data.confidence)
    nbins = 15
    bins = range(
        minconfidence, nextfloat(maxconfidence); length=nbins + 1
    )

    # Create figure.
    fig = Figure(; resolution=(800, 300))

    # Plot distribution of confidence estimates.
    for (key, df) in pairs(groupby(data, :train))
        istrain = key.train

        # Create new axis.
        ncol = istrain ? 1 : 2
        ax = Axis(
            fig[1, ncol];
            yticksvisible=istrain,
            yticklabelsvisible=istrain,
            title= (istrain ? "training"  : "validation") * " (n = $(nrow(df)))",
            (istrain ? (ylabel = "frequency",) : ())...,
        )

        # Plot histogram.
        hist!(ax, df.confidence; bins, normalization=:probability)
    end

    # Add common label for x and y axes.
    Label(fig[2, 1:2], "confidence"; valign=:top, padding=(0, 0, 0, 5))
    rowgap!(fig.layout, 1, 0)

    # Ensure that x and y axes are synced.
    linkaxes!(content(fig[1, 1]), content(fig[1, 2]))

    fig
end
savefig("confidence.pdf")

# Plot reliability diagram.
confidence = predictions_confidence.confidence[validxs]
outcomes = predictions_confidence.outcomes[validxs]
let confidence = confidence, outcomes = outcomes
    # Define custom axis scaling.
    xscale(x) = cbrt(1 - x)
    CairoMakie.Makie.inverse_transform(::typeof(xscale)) = x -> 1 - x^3
    CairoMakie.Makie.MakieLayout.defaultlimits(::typeof(xscale)) = (0.0, 1.0)
    function CairoMakie.Makie.MakieLayout.defined_interval(::typeof(xscale))
        return CairoMakie.Makie.OpenInterval(-Inf, Inf)
    end

    # Plot reliability diagram with consistency bars.
    Random.seed!(100)
    fig, ax, _ = reliability(
        confidence,
        outcomes;
        binning=EqualMass(; n=15),
        deviation=true,
        consistencybars=ConsistencyBars(),
        label="data",
        axis=(xscale=xscale, xticks=[0.75, 0.9, 0.99]),
        figure=(; resolution=(400, 300))
    )

    # Indicate ideal of zero deviation.
    hlines!(ax, 0; color=:black, linestyle=:dash, label="ideal")

    # Add legend.
    axislegend(ax; position=:lb)

    fig
end
savefig("reliability_diagram.pdf")

# Extract confidence, predictions, etc. on validation dataset.
predictions = map(RowVecs(Float64.(pdf(MLJ.predict(model, X[validxs, :]), levels(y))))) do p
    # Normalization ensures that values sum approximately to 1
    # after conversion to Float64
    normalize!(p, 1)
end
observations = levelcode.(y[validxs])

# Compute ECE estimates.
ece = ECE(UniformBinning(5), TotalVariation())
@show ece(confidence, outcomes)
@show ece(predictions, observations)

# Compute ECE estimates for different outcomes, distances, and number of bins.
let predictions = predictions, observations = observations
    nbins = 1:1:5
    distances = (TotalVariation(), Euclidean(), SqEuclidean())
    names = ["total variation", "Euclidean", "squared Euclidean"]

    n = length(nbins) * length(distances) # number of configurations
    estimates = Float64[]
    sizehint!(estimates, n)
    for d in distances, b in nbins
        push!(estimates, ECE(UniformBinning(b), d)(predictions, observations))
    end

    # Collect results in a data frame
    data = DataFrame(;
        nbins=repeat(nbins, length(distances)),
        distance=categorical(repeat(names; inner=length(nbins))),
        ece=estimates
    )

    # Plot estimates
    fig = Figure(; resolution=(800, 300))

    ax = Axis(fig[1, 1]; xlabel="number of bins (per class)", ylabel="ECE estimate", xticks=nbins)

    colors = CairoMakie.Makie.wong_colors(1)
    distances = levelcode.(data.distance)
    barplot!(ax, data.nbins, data.ece; dodge=distances, color=colors[distances])

    # Add legend.
    labels = levels(data.distance)
    elements = [PolyElement(; polycolor=colors[i]) for i in 1:length(labels)]
    Legend(fig[1, 2], elements, labels, "distance"; framevisible=false)

    fig
end
savefig("ece_estimates.pdf")

# Compute SKCE estimates.
kernel = RationalQuadraticKernel() ⊗ WhiteKernel()
skce = SKCE(kernel)
@show skce(predictions, observations)

skce = SKCE(kernel; unbiased=false)
@show skce(predictions, observations)

skce = SKCE(kernel; blocksize=5)
@show skce(predictions, observations)

# Compute SKCE estimates for different length scales.
lengthscales = exp10.(-3:1)
threshold = entropy([0.9, 0.1])
let predictions = predictions, observations = observations, lengthscales = lengthscales, threshold = threshold
    # Compute estimates based on all predictions.
    estimates = map(lengthscales) do lengthscale
        kernel = with_lengthscale(RationalQuadraticKernel(), lengthscale) ⊗ WhiteKernel()
        return SKCE(kernel)(predictions, observations)
    end

    # Plot estimates based on all predictions.
    fig = Figure(; resolution=(800, 300))
    xticks = (1:length(lengthscales), string.(lengthscales))
    ax_all = Axis(fig[1, 1]; title="all (n = $(length(predictions)))", ylabel="squared KCE estimate", xticks)
    barplot!(ax_all, 1:length(lengthscales), estimates)

    # Compute estimates based on low-entropy predictions.
    idxs = [i for (i, preds) in enumerate(predictions) if entropy(preds) <= threshold]
    predictions = predictions[idxs]
    observations = observations[idxs]
    estimates = map(lengthscales) do lengthscale
        kernel = with_lengthscale(RationalQuadraticKernel(), lengthscale) ⊗ WhiteKernel()
        return SKCE(kernel)(predictions, observations)
    end

    # Plot estimates based on low-entropy predictions.
    ax_low = Axis(
        fig[1, 2];
        title="low entropy (n = $(length(predictions)))",
        yticksvisible=false,
        yticklabelsvisible=false,
        xticks
    )
    barplot!(ax_low, 1:length(lengthscales), estimates)

    # Add common label.
    Label(fig[2, :], "length scale"; valign=:top, padding=(0, 0, 0, 5))
    rowgap!(fig.layout, 1, 0)

    # Sync axes.
    linkaxes!(ax_all, ax_low)

    fig
end
savefig("skce_estimates.pdf")

# Calibration tests.
Random.seed!(876)
@show AsymptoticSKCETest(kernel, predictions, observations)

test = ConsistencyTest(ece, predictions, observations)
@show pvalue(test; bootstrap_iters=10_000)

# Plot p-value estimates for different length scales.
let predictions = predictions, observations = observations, lengthscales = lengthscales, threshold = threshold
    Random.seed!(642)

    # Compute estimates based on all predictions.
    estimates = map(lengthscales) do lengthscale
        kernel = with_lengthscale(RationalQuadraticKernel(), lengthscale) ⊗ WhiteKernel()
        test = AsymptoticSKCETest(kernel, predictions, observations)
        return pvalue(test)
    end

    # Plot estimates based on all predictions.
    fig = Figure(; resolution=(800, 300))
    xticks = (1:length(lengthscales), string.(lengthscales))
    ax_all = Axis(
        fig[1, 1];
        title="all (n = $(length(predictions)))",
        ylabel="p-value estimate",
        xticks
    )
    barplot!(ax_all, 1:length(lengthscales), estimates)
    hlines!(ax_all, 0.05; linestyle=:dash, color=:red)

    # Compute estimates based on low-entropy predictions.
    idxs = [i for (i, preds) in enumerate(predictions) if entropy(preds) <= threshold]
    predictions = predictions[idxs]
    observations = observations[idxs]
    estimates = map(lengthscales) do lengthscale
        kernel = with_lengthscale(RationalQuadraticKernel(), lengthscale) ⊗ WhiteKernel()
        test = AsymptoticSKCETest(kernel, predictions, observations)
        return pvalue(test)
    end

    # Plot estimates based on low-entropy predictions.
    ax_low = Axis(
        fig[1, 2];
        title="low entropy (n = $(length(predictions)))",
        yticksvisible=false,
        yticklabelsvisible=false,
        xticks
    )
    barplot!(ax_low, 1:length(lengthscales), estimates)
    hlines!(ax_low, 0.05; linestyle=:dash, color=:red)

    # Add common label.
    Label(fig[2, :], "length scale"; valign=:top, padding=(0, 0, 0, 5))
    rowgap!(fig.layout, 1, 0)

    # Sync axes.
    linkaxes!(ax_all, ax_low)

    fig
end
savefig("pvalue_estimates.pdf")
