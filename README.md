# Talks

[![builds.sr.ht status](https://builds.sr.ht/~devmotion/talks/commits/site.svg)](https://builds.sr.ht/~devmotion/talks/commits/site?)

This repository is a collection of some of my talks.

## Table Of Contents

- November 2022:
  - [Calibration of probabilistic predictive models](https://talks.widmann.dev/2022/11/calibration.pdf) ([Gothenburg Statistics Seminar](https://www.chalmers.se/en/departments/math/research/seminar-series/statistics-seminar/Pages/default.aspx))
- March 2022:
  - [Calibration of probabilistic predictive models](https://talks.widmann.dev/2022/03/calibration.pdf) ([Machine Learning Journal Club of the Gatsby Unit at UCL](https://www.ucl.ac.uk/gatsby/))
- February 2022:
  - [Scientific computing with Julia](https://talks.widmann.dev/2022/02/Julia/) ([Polygon Math Club at the American University in Bulgaria](https://www.facebook.com/MathPolygonAUBG))
- July 2021:
  - [Calibration analysis of probabilistic models with Julia](https://talks.widmann.dev/2021/07/Calibration/) ([JuliaCon 2021](https://juliacon.org/2021/))
  - [EllipticalSliceSampling.jl: MCMC with Gaussian priors](https://talks.widmann.dev/2021/07/EllipticalSliceSampling/) ([JuliaCon 2021](https://juliacon.org/2021/))
  - [Probabilistic Modelling with Turing](https://talks.widmann.dev/2021/07/Turing/) ([Julia User Group Munich](https://www.meetup.com/Julia-User-Group-Munich/))
